﻿using UnityEngine;
using System.Collections;

public class Moon : Sun
{
    [Tooltip("Planet this planet orbits.")]
    [SerializeField]
    GameObject parentPlanet;

    [Tooltip("Speed this planet orbits its parent at.")]
    public float orbSpeed;

    [Tooltip("Distance between this planet and its parent")]
    public float parentDist;

    void Start()
    {
        InitializePosition();
        InitializeRadius();
    }

    void Update()
    {
        Rotate();
        Orbit();
    }

    public override void InitializePosition()
    {
        Vector3 parentPosition = parentPlanet.transform.position;
        float parentRadius = GetRadius(parentPlanet);
        float initPosOffset = parentRadius + parentDist + diameter / 2;
        Vector3 initPos = parentPosition + new Vector3(0, 0, initPosOffset);
        transform.position = initPos;
    }

    protected float GetRadius(GameObject sent)
    {
        return (sent.transform.localScale.x + sent.transform.localScale.y + sent.transform.localScale.z) / 3 / 2;
    }

    protected void Orbit()
    {
        transform.RotateAround(parentPlanet.transform.position, Vector3.up, orbSpeed * Time.deltaTime);
    }
}
